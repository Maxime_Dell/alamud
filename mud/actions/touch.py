from .action import Action2
from mud.events import TouchEvent

class TouchAction(Action2):
    EVENT = TouchEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "touch"