from .event import Event2

class TouchEvent(Event2):
    NAME = "touch"

    def perform(self):
        if not self.object.has_prop("touchable"):
            self.fail()
            return self.inform("touch.failed")
        self.inform("touch")
